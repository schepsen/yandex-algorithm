## PROJECT ##

* ID: **Y**andex!**A**LGORITHM 2016
* Contact: nikolaj@schepsen.eu

## DESCRIPTION ##

Yandex.Algorithm is an annual international programming championship organized by Russia’s leading search provider Yandex

## CHANGELOG ##

### Yandex!ALGORITHM 2016, updated @ 2016-05-23 ###

* solved Task [Q1-A](https://contest.yandex.ru/algorithm2016/contest/2498/problems/A)
* solved Task [Q1-A](https://contest.yandex.ru/algorithm2016/contest/2498/problems/B)

### Yandex!ALGORITHM 2016, updated @ 2016-06-11 ###

* solved Task [Q1-A](https://contest.yandex.ru/algorithm2016/contest/2540/problems/A)

