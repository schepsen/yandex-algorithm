#include <iostream>
#include <string>
#include <map>
#include <vector>

/*
 * Contest: Yandex.Algorithm 2016 (R2) (Task A)
 * URL: https://contest.yandex.ru/algorithm2016
 */

int main(int argc, char** argv)
{
    std::ios_base::sync_with_stdio(false);

    int n, m, answer = 0; char ch;

    std::string s; std::map<int, int> db;

    std::cin >> n >> m >> s;

    for (int i = 0, p, length; i < m; ++i)
    {
        std::cin >> length >> p;

        for (int j = 0; j < length; ++j)
        {
            std::cin >> ch; 
	    
	    if(db.count(ch)) db[ch] = std::min(db[ch], p); else db[ch] = p;	    
        }
    }

    for (int i = 0; i < n; ++i)
    {
        if(!db.count(s[i]))
        {
            std::cout << -1 << std::endl;

            return 0;
        }

        answer += db[s[i]];
    }

    std::cout << answer << std::endl; return 0;
}