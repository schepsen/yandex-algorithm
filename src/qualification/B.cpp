#include <iostream>
#include <cstdio>
#include <limits>

/*
 * Contest: Yandex.Algorithm 2016 (Q1) (Task B)
 * URL: https://contest.yandex.ru/algorithm2016
 */

int main(int argc, char** argv)
{
    std::ios_base::sync_with_stdio(false);
    long long v, n, index, distance;

    double phrase, opt = std::numeric_limits<double>::max(), t, tmp;

    std::cin >> v >> phrase >> n;

    for (int i = 1; i <= n; ++i)
    {
        std::cin >> distance >> t;
        if(((tmp = distance + v * (t + phrase)) >= 0) && (tmp < opt))
        {
            opt = tmp; index = i;
        }
    }

    printf("%0.5lf %lld\n", opt, index); return 0;
}
