#include <iostream>
#include <set>
#include <string>

/*
 * Contest: Yandex.Algorithm 2016 (Q1) (Task A)
 * URL: https://contest.yandex.ru/algorithm2016
 */

int main(int argc, char** argv)
{
    std::ios_base::sync_with_stdio(false);

    int n; std::cin >> n;
    std::string word;
    std::set<std::string> list;

    for (int i = 0; i < n; ++i)
    {
        std::cin >> word; list.insert(word);
    }

    for (auto it = list.begin(); it != list.end(); ++it)
    {
        int distance = 0;

        for (auto ptr = it; ptr != list.end(); ++ptr)
        {
            int current = 0;

            for (int i = 0; i < (*it).length(); ++i)
            {
                if ((*it)[i] != (*ptr)[i]) current++;
            }

            distance = std::max(current, distance);
        }

        if (distance <= 1) { std::cout << *it << std::endl; break; }
    }

    return 0;
}
